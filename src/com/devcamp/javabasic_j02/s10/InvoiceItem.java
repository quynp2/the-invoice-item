package com.devcamp.javabasic_j02.s10;

public class InvoiceItem {
    String id;
    String desc;
    int qty;
    double unitPrice;

    public InvoiceItem(String id, String desc, int qty, double unitPrice) {
        this.id = id;
        this.desc = desc;
        this.qty = qty;
        this.unitPrice = unitPrice;
    }

    public InvoiceItem() {
        this.id = "02CatFood";
        this.desc = "DO an cho meo";
        this.qty = 50;
        this.unitPrice = 100000;
    }

    public String getId() {
        return id;
    }

    public String getDesc() {
        return desc;
    }

    public int getQty() {
        return qty;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getTotal() {
        return unitPrice * qty;
    }

    @Override
    public String toString() {
        return String.format("InvoiceItem[ id = %s, desc = %s, qty=%s, unitPrice = %s]", id, desc, qty, unitPrice);
    }

}
